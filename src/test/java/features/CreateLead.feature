Feature: Create a Lead for Leaftaps
Background:
Given Open Chrome Browser
And Maximize Chrome Browser
And Set the timeout
And Enter the url
And Enter the username as DemoCSR
And Enter the password as crmsfa
And Click on Login button
And Verify Login is success
And Click crmsfa
And Click Leads
And Click CreateLead

Scenario: Positive Flow
And Enter Company Name as TestLeaf
And Enter First Name as Keerthana
And Enter Last Name as Aravindhan
When Click submit button
Then Verify Lead Created

Scenario: Negative Flow
And Enter First Name as Keerthana
And Enter Last Name as Aravindhan
When Click submit button
But Verify Lead Created

