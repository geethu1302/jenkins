package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadSteps {
	
	ChromeDriver driver;
	@Given("Open Chrome Browser")
	public void openChromeBrowser() {
	    System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		driver=new ChromeDriver();
	 //   throw new cucumber.api.PendingException();
	}

	@Given("Maximize Chrome Browser")
	public void maximizeChromeBrowser() {
		driver.manage().window().maximize();
	   // throw new cucumber.api.PendingException();
	}

	@Given("Set the timeout")
	public void setTheTimeout() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    //throw new cucumber.api.PendingException();
	}

	@Given("Enter the url")
	public void enterTheUrl() {
		driver.get("http://leaftaps.com/opentaps");
	//	throw new cucumber.api.PendingException();
	}

	@Given("Enter the username as (.*)")
	public void enterTheUsernameAsDemoCSR(String uname) {
		driver.findElementById("username").sendKeys(uname);
		
		//throw new cucumber.api.PendingException();
	}

	@Given("Enter the password as (.*)")
	public void enterThePasswordAsCrmsfa(String pwd) {
		driver.findElementById("password").sendKeys(pwd);
	    //throw new cucumber.api.PendingException();
	}

	@Given("Click on Login button")
	public void clickOnLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	    //throw new cucumber.api.PendingException();
	}

	@Given("Verify Login is success")
	public void verifyLoginIsSuccess() {
		System.out.println(driver.getTitle());
		//throw new cucumber.api.PendingException();
	}

	@Given("Click crmsfa")
	public void clickCrmsfa() {
	   driver.findElementByPartialLinkText("CRM/SFA").click();
	    //throw new cucumber.api.PendingException();
	}

	@Given("Click Leads")
	public void clickLeads() {
		driver.findElementByPartialLinkText("Leads").click();
		//throw new cucumber.api.PendingException();
	}

	@Given("Click CreateLead")
	public void clickCreateLead() {

		driver.findElementByPartialLinkText("Create Lead").click();
	    //throw new cucumber.api.PendingException();
	}

	@Given("Enter Company Name as (.*)")
	public void enterCompanyname(String cname) {
	   driver.findElementById("createLeadForm_companyName").sendKeys(cname);
	    //throw new cucumber.api.PendingException();
	}

	@Given("Enter First Name as (.*)")
	public void enterFirstName(String fname) {
	    driver.findElementById("createLeadForm_firstName").sendKeys(fname);
	  // throw new cucumber.api.PendingException();
	}

	@Given("Enter Last Name as (.*)")
	public void enterLastName(String lname) {
	   driver.findElementById("createLeadForm_lastName").sendKeys(lname);
	   // throw new cucumber.api.PendingException();
	}

	@When("Click submit button")
	public void clickSubmitButton() {
		driver.findElementByClassName("smallSubmit").click();
//		throw new cucumber.api.PendingException();
	}

	@Then("Verify Lead Created")
	public void verifyLeadCreated() {
	    //System.out.println(driver.getTitle());
	    if(driver.getTitle().equalsIgnoreCase("View Lead"))
	    		System.out.println("Lead created");
	    else
	    	System.out.println("Create Lead Failed");
	    
	//    throw new cucumber.api.PendingException();
	}
	
	@Then("Verify Lead Created Failed")
	public void createLeadFailed() {
		System.out.println("Lead created failed");
	}

}
