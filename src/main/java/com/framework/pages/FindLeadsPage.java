package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import com.framework.design.ProjectMethods;


public class FindLeadsPage extends ProjectMethods {

	@FindBy(how=How.XPATH,using="(//input[@name='firstName'])[3]") WebElement eleFirstName;
	@FindBy(how=How.XPATH,using="//button[text()='Find Leads']") WebElement eleFindLeadsButton;
	@FindBy(how=How.XPATH,using="(//a[@class='linktext'])[4]") WebElement  eleFirstLeadId;
	public FindLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	public FindLeadsPage enterFirstName(String data)
	{
		
		clearAndType(eleFirstName, data);
		return this;
	}
	
	public FindLeadsPage clickFindLeadsButton() {
		
		click(eleFindLeadsButton);
		return this;
	}
	
	public ViewLeadPage clickFirstLeadID() {
		
		clickWithNoSnap(eleFirstLeadId);
		return new ViewLeadPage();
	}
}
