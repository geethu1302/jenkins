package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	@FindBy(how=How.ID,using="createLeadForm_companyName") WebElement eleCompName;
	@FindBy(how=How.ID,using="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how=How.ID,using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how=How.CLASS_NAME,using="smallSubmit") WebElement eleSubmitLead;
	public CreateLeadPage() {
		PageFactory.initElements(driver,this);
	}
	
	public CreateLeadPage enterCompanyName(String compName) {
		clearAndType(eleCompName,compName );	
		return this;
	}
	
	public CreateLeadPage enterFirstName(String firstName) {
		clearAndType(eleFirstName, firstName);
		return this;
	}
	
	public CreateLeadPage enterLastName(String lastName) {
		clearAndType(eleLastName, lastName);
		return this;
	}
	
	public ViewLeadPage clickSubmitLead() {
		click(eleSubmitLead);
		return new ViewLeadPage();
	}
	
}
