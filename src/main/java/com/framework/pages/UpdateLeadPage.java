package com.framework.pages;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class UpdateLeadPage extends ProjectMethods {

	@FindBy(how=How.ID,using="updateLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how=How.XPATH,using="(//input[@name='submitButton'])[1]") WebElement eleUpdateLeadButton;
	public UpdateLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	public UpdateLeadPage enterCompanyName(String data) {
		
		clearAndType(eleCompanyName, data);
		return new UpdateLeadPage();
		
	}
	
	public ViewLeadPage clickUpdateLead() {
		click(eleUpdateLeadButton);
		return new ViewLeadPage();
	}
	
	
}
