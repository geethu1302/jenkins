package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {

	
	public ViewLeadPage() {
		PageFactory.initElements(driver,this);
	}
	
	@FindBy(how=How.XPATH,using="//span[@id='viewLead_firstName_sp']") WebElement eleFirstNameText;
	@FindBy(how=How.XPATH,using="(//a[@class='subMenuButton'])[3]") WebElement eleEditLeadButton;
	@FindBy(how=How.XPATH,using="//span[@id='viewLead_companyName_sp']") WebElement eleCompanyNameText;
	public ViewLeadPage verifyName(String data) 
	{
		verifyExactText(eleFirstNameText, data);
		return this;
		
	}
	public UpdateLeadPage clickEditLead() {
	
		click(eleEditLeadButton);
		return new UpdateLeadPage();
	}
	
	public ViewLeadPage verifyCompanyName(String data)
	{
		verifyPartialText(eleCompanyNameText,data);
		return new ViewLeadPage();
		
	}
}
