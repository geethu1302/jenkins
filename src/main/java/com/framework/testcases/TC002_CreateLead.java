package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.CreateLeadPage;
import com.framework.pages.HomePage;
import com.framework.pages.LoginPage;
import com.framework.pages.MyHomePage;
import com.framework.pages.MyLeadsPage;
import com.framework.pages.ViewLeadPage;

public class TC002_CreateLead extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName="TC002_CreateLead";
		testDescription="To create a lead in LeafTaps";
		testNodes="Leads";
		 author="Keerthana";
		 category="Smoke";
		 dataSheetName="TC002";
		
		
	}
	
	@Test(dataProvider="fetchData")
	
	public void CreateLead(String uname,String pwd,String cname,String fname,String lname) {
		new LoginPage().enterUsername(uname).enterPassword(pwd).clickLogin()
		.clickCrmsfa()
		.clickLeads()
		.clickCreateLeads()
		.enterCompanyName(cname)
		.enterFirstName(fname)
		.enterLastName(lname)
		.clickSubmitLead()
		//.verifyName(fname)
		;
		
	}
		
	
}
