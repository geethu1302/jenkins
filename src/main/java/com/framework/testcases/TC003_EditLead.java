package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC003_EditLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName="TC003_EditLead";
		testDescription="To edit a lead in LeafTaps";
		testNodes="Leads";
		 author="Keerthana";
		 category="Smoke";
		 dataSheetName="TC003";
		
		
	}
	
	@Test(dataProvider="fetchData")
	public void editLead(String uname,String pwd,String fname,String cname) {
		new LoginPage().enterUsername(uname).enterPassword(pwd).clickLogin()
		.clickCrmsfa()
		.clickLeads()
		.clickFindLeads()
		.enterFirstName(fname)
		.clickFindLeadsButton()
		.clickFirstLeadID()
		.clickEditLead()
		.enterCompanyName(cname)
		.clickUpdateLead()
		.verifyCompanyName(cname);
		
	}
}
